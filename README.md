# DeadInternetTheoryWebRing

A list of chat rooms and chans and forums that aren't controlled by big tech and allow free speech.

Contribute to this list by making a pull request.

## Non-big tech controlled chats ✔️
+ [Official dead internet theory matrix room](https://matrix.to/#/#deadinternettheory:matrix.org)
+ Official dead internet theory IRC channel: irc.rizon.net #deadinternettheory


## Dead sites RIP ⚰️
+ liveleak.com
+ 8chan
+ 8kun
+ yahoo answers
+ crypto.cat